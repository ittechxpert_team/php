<?php
include "./db/dbconnent.php";
include "./app/lib/order.php";

// if (isset($_GET['id'])) {
//     $id =  $_POST['id'];
//     $delete   = "DELETE FROM `php_dummy` WHERE id = {$id}";
//     $query = $conn->query($delete);
//     if ($conn->query($delete) == TRUE) {
//         echo "yes";
//         header('Location:list.php');
//     } else {
//         echo "Error deleting record:";
//     }
// }
?>
<head>
    <title>User List Page</title>
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }
        td,
        th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }

        a {
            text-decoration: none;
            color: white;
            text-transform: capitalize;
        }

        .button {
            background-color: gray;
            padding: 6px;
        }

        .link {
            color: black !important;
            text-decoration: underline;
        }
        .status p {
            font-size: 14px !important;
            margin: 0px auto !important;
            background: antiquewhite;
            text-align: center;
            width: 73px;
            border-radius: 14px;
            cursor: pointer;
            padding: 3px;
        }
        .pending {
            background: green !important;
            color: white;
        }
        .approved {
            background: gray !important;
            color: white;
        }
        .chipped {
            background: #ffc9e1 !important;
        }
        .cancelled {
            background: aliceblue !important;
        }
        button#dropdownMenu2 {
            border: none;
            border-radius: 0px;
            margin: 18px 10px;
        }
        ul.dropdown-menu.show {
            border: 1px solid #e4e4e4;
            border-radius: 0px;
        }
    </style>
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
</head>
<?php
$wherecond = '';
$name = "";
if(isset($_GET['name']) && !empty($_GET['name'])){
    // $wherecond = 'where vendor_name like '.$_GET['name'] . '';
    $wherecond = 'where vendor_name like "%'.$_GET['name'].'%"';
}

if(isset($_GET['status'] ) && !empty($_GET['status'])){
    $wherecond = 'where status like "'.$_GET['status'].'"';
}

// echo $name;

$status = "";  
$sql = "SELECT id, vendor_id, vendor_name, status  FROM  orders_vendors "  . $wherecond ;
// echo  $sql; 
// $sql = "SELECT order_id FROM orders_vendors UNION SELECT order_id FROM orders";



$result = $conn->query($sql);
// print_r($result);
// exit();
$status = ""; 
?>

<div class="dropdown w-25 m-auto">
    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-bs-toggle="dropdown" aria-expanded="false">
        Dropdown
    </button>
    <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
        <li><a href="list.php?status=1" class="dropdown-item <?php if ($status === "1") echo "selected"; ?>">Pending</a></li>
        <li><a href="list.php?status=2" class="dropdown-item <?php if ($status === "2") echo "selected"; ?>">Waiting for Confirmation</a></li>
        <li><a href="list.php?status=3" class="dropdown-item <?php if ($status === "3") echo "selected"; ?>">Approved</a></li>
        <li><a href="list.php?status=4" class="dropdown-item <?php if ($status === "4") echo "selected"; ?>">In Production</a></li>
        <li><a href="list.php?status=5" class="dropdown-item <?php if ($status === "5") echo "selected"; ?>">Shipped</a></li>
        <li><a href="list.php?status=6" class="dropdown-item <?php if ($status === "6") echo "selected"; ?>">Cancelled</a></li>
    </ul>
</div>

<form method="get" action="list.php" >
  <input type="search" id="gsearch" name="name">
  <input type="submit" value = "Filter">
</form>


<a class="link" href="add.php">Add User</a>
<table class="recordListing" > 
    <tr>
        <th>Id</th>
        <th>Vendor Id</th>
        <th>Vendor Name</th>
        <th>Vendor status</th>
        <th>Detele/Update</th>
    </tr>
    <?php
    while ($row = $result->fetch_assoc()) {
    ?>
        <tr>
            <td><?php echo $row['id']; ?></td>
            <td><?php echo $row['vendor_id']; ?></td>
            <td><?php echo $row['vendor_name']; ?></td>
            <td><?php echo getvendorstatus($row['status']); ?></td>
            <td>
                <button style= "cursor: pointer;"  class="button delete-btn" data-id="<?php echo $row['id'];?>" >Delete</button>
                 <a  style= "cursor:pointer;"  class="button " href ="update.php?id=<?php echo $row['id'];?>">Update</a>
            </td> 
        </tr>
    <?php
    } ?>
</table>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script type="text/javascript" src="./assest/js/user.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</body>

</html>    

