$(document).on("submit", "#addForm", function (e) {
  e.preventDefault();
  $.ajax({
    method: "POST",
    url: "ajax/user-aj.php?code=addUser",
    data: $(this).serialize(),
    beforeSend: function () {
      $(".form-submit").attr("disabled", true);
      $(".form-submit").text("Processing...");
    },
    complete: function () {
      $(".form-submit").attr("disabled", false);
      $(".form-submit").text("Submit");
    },
    success: function (d) {
      let e = JSON.parse(d);
      if (e.success == 1) {
        alert(e.message);
      } else {
        alert(e.err);
      }
      // console.log(d);
    },
  });
});


$(document).ready(function () {
  $(".recordListing").on("click", ".delete-btn", function () {
    var delete_id = $(this).data("id");
    // var element = this
      $.ajax({
        url: "ajax/user-aj.php?code=deleteUser", // Replace with your server-side delete script
        type: "POST",
        data: { id: delete_id },
        // dataType: "json", 
        success: function (d) {
          var d = JSON.parse(d);
          if (d.success == 1) {
            alert(d.message);
            location.reload();
          } else {
            alert(d.err);
          }
          console.log(d);
        },
      });
  });
});

$(document).on("submit", ".Update-form", function (e) {
  e.preventDefault();
  var formData = $(this).serialize();
  $.ajax({
    url: "ajax/user-aj.php?code=updateUser", // Replace with your server-side update script
    type: "POST",
    data: formData,
    success: function (r) {
      console.log(r);
      var r = '{"success": 1, "message": "Data updated successfully"}';
      var e = JSON.parse(r);
      if (e.success == 1) {
        alert(e.message);
      } else {
        alert(e.err);
      }
    },
  });
});



