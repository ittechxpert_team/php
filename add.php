<?php
include "./db/dbconnent.php";

// $id =  $name = $email = $password = $gender = $number = "";
// $nameErr = $emailErr = $passwordErr = $genderErr = $numberErr = "";
// $val = "1";

// if (isset($_POST['submit_data'])) {
//     if ($_POST['name'] == "") {
//         $nameErr = "Please Enter Your Name.";
//     } else {
//         $name = $_POST['name'];
//     }

//     if ($_POST['email'] == "") {
//         $emailErr = "Please Enter Your Email.";
//     } else {
//         $email = $_POST['email'];
//         if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
//             $emailErr = "Invalid email format";
//         }
//     }
//     if (empty($_POST['password'])) {
//         $passwordErr = "Please Enter Your Password.";
//     } else {
//         $password = $_POST['password'];
//     }
//     if (empty($_POST["gender"])) {
//         $genderErr = "Please Enter Your Gender.";
//     } else {
//         $gender = $_POST['gender'];
//     }

//     if (empty($_POST['number'])) {
//         $numberErr = "Please Enter Your Number.";
//     } else {
//         $number = $_POST['number'];
//     }
//     if ($val == 1) {
//         $sql = "INSERT INTO php_dummy (name, email, password, gender, number) VALUES ('{$name}', '{$email}', '{$password}', '{$gender}', '{$number}')";
//         echo $sql;
//         if ($conn->query($sql) === TRUE) {
//             echo "Data inserted successfully";
//         }
//     }
// }
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
    </head>

    <body>
        <a href="list.php">User list </a><br><br>

        <!-- <form method="post">
            <label for="name">Name</label>
            <br>
            <input type="text" name="name" value="<?php echo $name; ?>" placeholder="enter your name ">
            <span style="color:red"><?php echo  $nameErr; ?></span>
            <br>
            <label for="email">Email</label>
            <br>
            <input type="text" name="email" value="<?php echo $email; ?>" placeholder="Enter you email">
            <span style="color:red"><?php echo  $emailErr; ?></span>
            <br>
            <label for="pss">PassWord</label>
            <br>
            <input type="passWord" name="password" value="<?php echo $password; ?>" placeholder="Enter you Password">
            <span style="color:red"><?php echo  $passwordErr; ?></span>
            <br>
            <label for="number">Phone Number</label>
            <br>
            <input type="text" name="number" value="<?php echo $number; ?>" placeholder="Enter you Phone Number">
            <span style="color:red"><?php echo  $numberErr; ?></span>
            <br>
            <input type="radio" name="gender" value="female" >Female
            <input type="radio" name="gender" value="male">Male
            <input type="radio" name="gender" value="other">Other
            <span style="color:red"><?php echo  $genderErr; ?></span>
            <br>
            <br>
            <input type="submit" name="submit_data">
        </form> -->
        <div class="container">
            <!-- form start -->
            <form action="" method="post" id="addForm"  class="w-50 m-auto">
                <div class="form-group mb-2">
                    <label for="name">Name</label>
                    <input type="text" class="form-control"  name="name"   id="name" placeholder="Enter your name">
                </div>
                <div class="form-group mb-2">
                    <label for="email">Email </label>
                    <input type="text" class="form-control"  name="email" id="email"  placeholder="Enter your email">
                </div>
                <div class="form-group mb-2">
                    <label for="password">Password</label>
                    <input type="password" class="form-control"  name="password" id="password"  placeholder="Enter your password">
                </div>
                <div class="form-group mb-2">
                    <label for="number">Number</label>
                    <input type="text" class="form-control"  name="number" id="number"  placeholder="Enter you number">
                </div>
                <div class="form-group mb-2">
                    <label for="gender">Male</label>
                        <input class="form-check-input" type="radio" name="gender" value="male" id="gender">
                </div>   
                <div class="form-group mb-2">
                    <label for="gender">Female</label>
                    <input class="form-check-input" type="radio" name="gender" value="female" id="gender">
                </div>             
                <div class="form-group mb-2">
                    <label for="gender">Other</label>
                    <input class="form-check-input" type="radio" name="gender" value="female" id="gender">
                </div>                                                             
                <button type="submit"  id="" class="btn btn-primary form-submit">Submit</button>
            </form>
            <!-- form end -->

        </div>
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script type="text/javascript" src="./assest/js/user.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    </body>
</html>


