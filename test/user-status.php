<?php 
include "../db/dbconnent.php";

// $sql =  "SELECT order_id , store_name , store_url , status FROM `orders`" ;
// $sql = "SELECT  orders.status, orders_vendors.status FROM orders LEFT JOIN orders_vendors ON orders.order_id = orders_vendors.order_id";
$sql = "SELECT orders.order_id, orders.store_name,orders.store_url, orders_vendors.vendor_name, orders_vendors.status FROM orders LEFT JOIN orders_vendors ON orders.order_id = orders_vendors.order_id";
$result = $conn->query($sql);
// print_r($result);

// $data = array();
// foreach ($result as $row){
//     echo "<pre>";
//     // print_r($row);
//     $data[] = $row;
// }
// print_r($data);
?>
<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<title>Docment</title>
<!-- Bootstrap CSS -->
<!-- <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css"> -->
<!-- jQuery -->
<!-- jQuery UI -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
<link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
    />
</head>
<style>
    .table>tbody>tr>td{
        text-align: justify;
    }
    .table thead th{
        text-align: left;
    }
</style>
<body>

<div class="container-fluid mt-4">
    <div class="row header" style="text-align:center;color:green">
        <h2 style=color:red >Two Table Data</h2>      
        <table class="table" id= "tableData">
            <thead>
            <tr>
                <th>Order id</th>
                <th>Store Name</th>
                <th>Store Url</th>
                <th>Vendor Name</th>
                <th>status</th>
            </tr>
            </thead>
            <tbody>
                <?php 
                 foreach ($result as $row){
                    ?>
                <tr>
                    <td><?php echo $row['order_id']; ?></td>
                    <td><?php echo $row['store_name']; ?></td>
                    <td><?php echo $row['store_url']; ?></td>
                    <td><?php echo $row['vendor_name']; ?></td>
                    <td><?php echo $row['status']; ?></td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>	
