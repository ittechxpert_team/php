<?php
include "../db/dbconnent.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<!-- jQuery -->
<!-- jQuery UI -->
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
<link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
    />
</head>
<style>
    .table>tbody>tr>td{
        text-align: justify;
    }
</style>
<body>
<div class="container-fluid mt-4">
    <div class="row header" style="text-align:center;color:green">
        <h2 style=color:red >Two Table Data</h2>      
        <table class="table" id= "tableData">
            <thead>
            <tr>
                <th>Order id</th>
                <th>Store Name</th>
                <th>Store Url</th>
                <th>Ship Name</th>
                <th>Ship Last</th>
                <th>Ship City</th>
                <th>Bill Emailaddress</th>
                <th>Vendor Name</th>
            </tr>
            </thead>
            <tbody>
                <?php 
                $sql = "SELECT * FROM orders JOIN orders_vendors ON orders.order_id = orders_vendors.order_id";
                // $sql = "SELECT * FROM orders JOIN orders_vendors"
                $result = $conn->query($sql);
                foreach ($result as $row){
            ?>
                    <tr>
                        <td><?php echo $row['order_id']; ?></td>
                        <td><?php echo $row['store_name']; ?></td>
                        <td><?php echo $row['store_url'];?></td>
                        <td><?php echo $row['ship_first_name'];?></td>
                        <td><?php echo $row['ship_last_name'];?></td>
                        <td><?php echo $row['ship_city'];?></td>
                        <td><?php echo $row['ship_emailaddress'];?></td>
                        <td><?php echo $row['vendor_name'];?></td>
                        <!-- <td>
                            <a href="test.php?=id<?php echo $row['order_id']; ?>">Delete</a>
                            <a href="test.php?=id<?php echo $row['order_id']; ?>">Update</a>
                        </td> -->
                    </tr>
            <?php }?>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>	
