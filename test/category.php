<?php
include "../db/dbconnent.php";
$sql = 'SELECT * FROM `catalog_category` WHERE parent_id = 0';
$result = $conn->query($sql);
$allCat = array(); 
foreach ($result as $data )
{
    $allCat[] = $data;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<!-- jQuery -->
<!-- jQuery UI -->
<title>Docment</title>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
<link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
    />
</head>
<style>
    .table>tbody>tr>td{
        text-align: justify;
    }
</style>
<body>
<div class="container mt-5">
    <div class="row header" style="text-align:center;color:green">
        <table class="table mt-5"  id= "tableData">
            <thead>
            <tr>
                <th>id</th>
                <th>Category Name</th>
                <th>Sort Name</th>
                <th>Parent Category</th>
                <th>Status</th>
                <th>Edit</th>
            </tr>
            </thead>
            <tbody>
                <?php
                    foreach ($result as $row){
                    ?>
                    <tr>
                        <td><?php echo $row['id']?></td>
                        <td><?php echo $row['category_name']?></td>
                        <td><?php echo $row['sort_name']?></td>
                        <td>
                            <?php foreach ($allCat as $cat){
                                if($cat['id'] == $row['parent_id']){
                                    echo  $cat['category_name']; 
                                }
                            }?>
                        </td>
                        <td>
                            <?php 
                                if($row['status'] == 1){
                                    echo "<div class='enable badge bg-success'> Enable " . $row['status'] . "</div>" ;
                                } else {
                                    echo "<div class='disable badge bg-danger'>  Disable " . $row['status'] . "</div>" ;
                                }
                            ?>
                        </td>
                        <td>
                            <a href="category-update.php?id=<?php echo $row['id']?>" class="badge bg-success">edit</a>
                        </td>
                    </tr>
                   <?php 
                    }
                  ?>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>	
