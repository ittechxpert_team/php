<?php
include "../db/dbconnent.php";
?>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
</head>

<div class="container">
    <div class="dropdown mt-5">
        <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
            Search Name 
        </a>
            <ul role="menu" class="dropdown-menu">
                <li class="all-filter"><p class='order_id'>Order Id</p></li>
                <li class="all-filter"><p class='vendor-name'>Vendor Name</p></li>
                <li class="all-filter"><p class='ship-first-name'>Customer Name</p></li>
                <li class="all-filter"><p class='website'>Website</p></li>
            </ul>
    </div>
    <form method="get" action="../test/status.php" >
        <div id="all-filter-input" style="margin-right:10px;">
            <input type="text" id="id-filter" name="name" required placeholder="Filter by Name">
        </div>
        <input type="submit" value = "Filter">
    </form>
    <div id="demo"></div>
</div>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>

$(".all-filter").click(function(){
    var filterClass = $(this).find('p').attr('class');
    var html = '';
    switch (filterClass) {
        case 'order_id':
            html = '<input type="text" id="id-filter" name="order_id" required placeholder="Filter by  Order Id">';
            break;
        case 'vendor-name':
            html = '<input type="text" id="id-filter" name="vendor_name" required placeholder="Filter by Vendor Name">';
            break;
        case 'ship-first-name':
            html = '<input type="text" id="id-filter" name="ship_first_name" required placeholder="Filter by Customer Name">';
            break;
        case 'website':
            html = '<input type="text" id="id-filter" name="store_name" required placeholder="Filter by Website">';
            break;
        default:
            html = '<input type="text" id="id-filter" name="name" required placeholder="Filter by Name">';
    }
    $('#all-filter-input').html(html);
});
</script>

