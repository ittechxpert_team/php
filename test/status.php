<?php
include "../db/dbconnent.php";
include "./user-status-order.php";
include './search-name-status.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Docment</title>
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"> -->
<!-- Bootstrap CSS -->
<!-- jQuery -->
<!-- jQuery UI -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
  <!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script> -->

    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
    />
    <style>
    table#tableData {
        text-align: left;
    }
    .dropdown{
        width: 50%;
        margin:auto;
    }
    button#dropdownMenuButton1 {
        border: none;
        border-radius: 0px;
        margin: 18px 10px;
    }
    ul.dropdown-menu.show {
        border: 1px solid #e4e4e4;
        border-radius: 0px;
    }
    .pending {
        background-color: orange;
        color:white;
         width: 80px;
        text-align: center;
        cursor: pointer;
        border-radius: 15px;
    }
    .waiting {
        background-color: blue;
        color:white;
        width: 80px;
        text-align: center; 
        cursor: pointer;
        border-radius: 15px;
   }
    .approved {
        background-color: green;
        color:white;
        width:80px ;
        text-align:center;  
        cursor: pointer;
        border-radius: 15px;  
    }
    .in {
        background-color: purple;
        color:white;
        width:80px ;
        text-align:center;
        cursor: pointer;
        border-radius: 15px;
    }
    .shipped {
        background-color: teal;
        color:white;
        width:80px ;
        text-align:center;
        cursor: pointer;
        border-radius: 15px;  
    }
    .cancelled {
        background-color: red;
        color:white;
        width:80px ;
        text-align:center;
        cursor: pointer;
        border-radius: 15px;
    }
    .dropdown-menu.show {
        display: block;
        padding: 5px 17px;
    }
    li.all-filter:hover {
        background: #efefef;
    }
</style>
</head>
<?php 
$wherecond = "";
$name = "";
$vendor_name = "";



// switch ($wherecond) {
//   case "name":
//     $wherecond = 'where orve.vendor_name like "%'.$_GET['name'].'%"';
//     break;
//   case "status":
//     $wherecond = 'Where  orve.status like "' . $_GET['status'].'"';
//     break;
//   case "order_id":
//     $wherecond = 'Where  orve.order_id like "' . $_GET['order_id'].'"';
//     break;
//   case "ship_first_name":
//     $wherecond = 'Where  ord.ship_first_name like "' . $_GET['ship_first_name'].'"';
//     break;
//   case "vendor_name":
//     $wherecond = 'where orve.vendor_name  like "%'. $_GET['vendor_name'].'%"';
//     break;
//   case "store_name":
//     $wherecond = 'where ord.store_name  like "' . $_GET['store_name'].'"';
//     break;
    
//   default:
//     echo "Very bad";
// }




if(isset($_GET['name']) && !empty($_GET['name'])){
    // $wherecond = 'where vendor_name like '.$_GET['name'] . '';
    $wherecond = 'where orve.vendor_name like "%'.$_GET['name'].'%"';
}
if(isset($_GET['status']) && !empty($_GET['status'])){
    $wherecond = 'Where  orve.status like "' . $_GET['status'].'"';
}

if(isset($_GET['order_id']) && !empty($_GET['order_id'])){
    $wherecond = 'Where  orve.order_id like "' . $_GET['order_id'].'"';
}

if(isset($_GET['ship_first_name']) && !empty($_GET['ship_first_name'])){
    $wherecond = 'Where  ord.ship_first_name like "' . $_GET['ship_first_name'].'"';
}


if(isset($_GET['vendor_name']) && !empty($_GET['vendor_name'])){
    $wherecond = 'where orve.vendor_name  like "%'. $_GET['vendor_name'].'%"';
}

if(isset($_GET['store_name']) && !empty($_GET['store_name'])){
    $wherecond = 'where ord.store_name  like "' . $_GET['store_name'].'"';
}

$status = "";

$sql = "SELECT orve.order_id , orve.vendor_name , orve.add_date , orve.status, ord.order_id , ord.store_name , ord.ship_first_name   FROM orders_vendors as orve JOIN orders as ord ON orve.order_id = ord.order_id" . " " . $wherecond ;
// $sql = "SELECT order_id,vendor_id,add_date, status,vendor_name FROM `orders_vendors`" . $wherecond;
// echo $sql;
$result = $conn->query($sql);
?>
<body>
    <div class="container-fluid mt-4"> 
            <div class="dropdown">
            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                Dropdown button
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                <li><a class="dropdown-item" <?php if ($status === "1") echo "selected"; ?> href="status.php?status=1">Pending</a></li>
                <li><a class="dropdown-item" <?php if ($status === "2") echo "selected"; ?> href="status.php?status=2">Waiting for Confimation</a></li>
                <li><a class="dropdown-item"  <?php if ($status === "3") echo "selected"; ?>href="status.php?status=3">Approved</a></li>
                <li><a class="dropdown-item" <?php if ($status === "4") echo "selected"; ?> href="status.php?status=4">In Production</a></li>
                <li><a class="dropdown-item" <?php if ($status === "4") echo "selected"; ?> href="status.php?status=5">Shipped</a></li>
                <li><a class="dropdown-item" <?php if ($status === "6") echo "selected"; ?> href="status.php?status=6">Cancelled</a></li>
            </ul>
        </div>
        <div class="row header" style="text-align:center;color:green">
            <table class="table" id= "tableData">
                <thead>
                <tr>
                    <th>Order  Id</th>
                    <th>Website</th>
                    <th>Vendor Name</th>
                    <th> Customer Name</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                    <?php foreach ($result as $row){?> 
                    <tr>
                        <td><?php echo $row['order_id'];?></td>
                        <td><?php echo $row['store_name'];?></td>
                        <td><?php echo $row['vendor_name'];?></td>
                        <td><?php echo $row['ship_first_name'];?></td>
                        <td><?php echo UserDataStatus($row['status']);?></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</body>
</html>	
